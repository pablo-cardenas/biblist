package App::biblist;

use 5.006;
use strict;
use warnings;
use Text::BibTeX;

=head1 NAME

App::biblist - The great new App::biblist!

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';


=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use App::biblist;

    my $foo = App::biblist->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head2 biburl

=cut

sub biburl {
    my $bibfile = Text::BibTeX::File->new($ARGV[0]);

    while ( my $entry = Text::BibTeX::Entry->new($bibfile) ) {
        my $url = get_url($entry);
        if ( defined($url) ) {
            print( "$url - ", join( " - ", details($entry) ) . "\n" );
        }
    }
}

=head2 bibopen

=cut

sub bibopen {
    my $bibfile = Text::BibTeX::File->new($ARGV[0]);

    while ( my $entry = Text::BibTeX::Entry->new($bibfile) ) {
        my $key = $entry->key();
        if ( defined($key) ) {
            print( "$key - ", join( " - ", details($entry) ) . "\n" );
        }
    }
}

=head2 details

=cut

sub details {
    my ($entry) = @_;
    my @fields_names = ('title', 'author', 'journal', 'year');
    my @fields_exists = grep {$entry -> exists($_)} @fields_names;
    my @fields_values = map { $entry->get($_) } @fields_exists;

    return @fields_values;
}

=head2 get_url

=cut

sub get_url {
    my ($entry) = @_;

    if ($entry->exists('doi')) {
        return $entry->get('doi');
    } elsif ($entry->exists('url')){
        return $entry->get('url');
    } else {
        return undef;
    }
}


=head1 AUTHOR

Pablo Cárdenas, C<< <pablo.cardenas at imca.edu.pe> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-biblist at rt.cpan.org>, or through
the web interface at L<https://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-biblist>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::biblist


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-biblist>

=item * CPAN Ratings

L<https://cpanratings.perl.org/d/App-biblist>

=item * Search CPAN

L<https://metacpan.org/release/App-biblist>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2022 by Pablo Cárdenas.

This is free software, licensed under:

  The Artistic License 2.0 (GPL Compatible)


=cut

1; # End of App::biblist
